var client = require ("./lib/client.js");
var server = require("./lib/server.js");

module.exports = {
  client: client,
  server: server
}
